# Development

## Clone repository
```
$ git clone --recursive git@bitbucket.org:getunik/getunikfrontendboilerplate.git yourprojectname
```
This will clone all sources including our kirby starterkit and kirby & panel as submodules

## Install node modules for gulp

```
$ npm install
```

## Install dependencies with bower

```
$ bower install
```

## Setup Gulp Configuration
Checkout gulpfile.js and tweak the config parameters there.

## Gulp Default Task
```
$ gulp
```
This will start a local on-demand PHP Server with BrowserSync. The LESS/JS/PHP Template files will be watched for changes and CSS will be re-generated on save. The /dist folder will be served by default.

## Configuring your Environment
The Kirby installation in this project comes with its own little _environment detection and configuration_ mechanism.
This allows the developer to have a different configuration for development and production systems - which makes life much easier.

By default this mechanism assumes that it is running in a production environment. All the settings in the `site/config/config.php` file are production settings.
For development, an additional environment specific file `config.[env].php` is loaded after the default configuration which allows settings to be overwritten.

The environment detection relies on the existence of an environment variable `KIRBY_ENV` on the system that is running the Kirby instance.
For a local development machine, the value for this variable should be set to `local`.

Add the following line to the end of your local `.bashrc` file if you are using bash or do the equivalent configuration of your system.
```
export KIRBY_ENV=local
```

## How to add new Javascript Libraries / Dependencies
```
$ bower install --save-dev jquery.plugin.foo.bar
```
Please add your new js dependency to the config.jsFiles[] array inside gulpfile.js as well. Then re-generate the js includes with:

```
$ gulp js-replace-dev
```

# Creating Projects based on Boilerplate
When starting a new project from the Boilerplate template, there are a couple of things you need to do before you really can get started. You start by cloning the Boilerplate repository and replacing the git remote _origin_ with your own project git repository.

```
$ git clone --recursive git@bitbucket.org:getunik/getunikfrontendboilerplate.git yourprojectname
$ git remote remove origin
$ git remote add origin git@bitbucket.org:getunik/yourprojectrepo.git
```

## Content-un-giting
### Removing existing content and accounts from git
Remove content/accounts from git repository without deleting the actual local files

```bash
git rm -rf --cached kirby/content
git rm -rf --cached kirby/site/accounts
git status
git commit -m 'removed content'
```

### Update .gitignore file
* update the .gitignore file to ignore content and accounts

```
# Kirby Content Files
# Uncomment these if and when content is managed outside of git
kirby/content/*
!kirby/content/.gitkeep
kirby/site/accounts/*
!kirby/site/accounts/.gitkeep
```
```bash
git add .gitignore
git commit -m 'updated .gitignore'
```

### Adjust the build/deployment process
* if necessary, adjust the build/deployment (gulpfile.js) process to exclude content with sensitive user information

* Modify the `plainRsyncOptions:` accordingly (file, folder or path).

```bash
git push
```

## Clearing Boilerplate Tags
Once you have _forked_ off from the Boilerplate project, it usually makes sense to make this official by removing all the boilerplate tags from the forked repository. This can easily be done with the git command line.
```bash
# delete remote tags first
$ git tag | xargs git push --delete origin
# then delete them locally
$ git tag | xargs git tag -d
```

# Gulp / Build Features

## Deployment via rsync
There is a nice deployment task too - checkout the config object inside gulpfile.js and setup your server settings. For every deployment target (_dev_, _prod_, ...), you have to define a couple of things in the global `env` variable.

```
var env = {
  dev: {
    user: 'getunik',
    host: 'guroot.nine.ch',
    // this path MUST end with a trailing slash
    path: '/home/getunik/www/dev.getunik.net/myproject/',
  },
};
```

Your environment name can be anything you want, but generally you should stick to the _well known_ ones (dev, stage, prod). For every environment defined this way, you'll get a couple of tasks that should help with deployment.


```bash
# deploy code to target MYENV
$ gulp deploy:MYENV
```

```bash
# fetches all Kirby content from target MYENV
$ gulp content:pull:MYENV
# updates MYENV target content with local content
$ gulp content:push:MYENV
```

```bash
# fetches all Kirby accounts from target MYENV
$ gulp accounts:pull:MYENV
# updates MYENV target accounts with local content
$ gulp accounts:push:MYENV
```
