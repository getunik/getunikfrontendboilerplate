<?php snippet('html-header') ?>

	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-8">

				<h1><?php echo $page->mainTitle()->html() ?></h1>

				<p class="lead">
					<?php echo $page->lead()->html() ?>
				</p>

				<?php echo $page->text()->kirbytext() ?>

			</div><!-- /.col-xs-12 -->
			<div class="col-xs-12 col-md-3 col-md-offset-1">

				<h3><?php echo $page->sidebarTitle() ?></h3>
				<?php echo $page->sidebarContent()->kirbytext() ?>

			</div><!-- /.col-xs-12 col-md-4 -->
		</div><!-- /.row -->
	</div><!-- /.container -->

<?php snippet('html-footer') ?>
