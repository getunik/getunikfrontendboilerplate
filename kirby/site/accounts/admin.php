<?php if(!defined('KIRBY')) exit ?>

username: admin
email: antonio.lopez@getunik.com
password: >
  $2a$10$dOcLOc/Nb05BOlNvl1c6gu0RzpbEKH6kgC7n5/nuXiksux7FaVFYm
language: en
role: admin
history:
  - basic-page
  - home
