<?php

// example echo l::get('submit')

l::set('language', 'Language');

l::set('leave-blank', 'Please leave this field blank');
l::set('registration-intern', 'Please use the following form for the event - registration.');
l::set('to', 'to');

l::set('contact', 'Contact');
l::set('pages', 'Pages');
l::set('service', 'Service');
l::set('network', 'Network');
l::set('download', 'Download');

l::set('cancel', 'Cancel');

// Search
l::set('search', 'Search');
l::set('search-nothing-found', 'The search returned no results.');

// Uniform
l::set('uniform-filled-potty', 'The form field that is supposed to be empty was filled. In case you are not a spam-bot, please try again leaving the field blank.');
l::set('uniform-fields-required', 'Please fill in all required fields.');
l::set('uniform-fields-not-valid', 'Some fields do not contain valid data.');

l::set('uniform-email-subject', 'Message from the web form');
l::set('uniform-email-success', 'Thank you, the form was sent successfully.');
l::set('uniform-email-error', 'There was an error sending the form:');
l::set('uniform-email-copy', ''); // Copy:

l::set('uniform-calc-plus', 'plus');

l::set('uniform-log-success', 'The log entry was successfully created.');
l::set('uniform-log-error', 'There was an error while writing to the logfile.');

l::set('uniform-login-error', 'Wrong username or password.');
l::set('uniform-login-success', 'Login successful.');

l::set('uniform-webhook-success', 'Calling webhook successful.');
l::set('uniform-webhook-error', 'There was an error calling the webhook: ');

l::set('uniform-email-select-error', 'Invalid recipient.');

// Contact
l::set('contact-form', 'Contact form');
l::set('company', 'Company');
l::set('salutation', 'Salutation');
l::set('name', 'Name');
l::set('surname', 'Surname');
l::set('phone', 'Phone');
l::set('email-address', 'Email Address');
l::set('message', 'Message');
l::set('submit', 'Submit');
l::set('location', 'Location');

?>
