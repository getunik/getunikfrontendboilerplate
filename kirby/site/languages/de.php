<?php

// example echo l::get('submit')

l::set('language', 'Sprache');

l::set('leave-blank', 'Bitte dieses Feld leer lassen');
l::set('registration', 'Anmeldung');
l::set('registration-intern', 'Bitte melden Sie sich mit folgendem Formular für die Veranstaltung an.');
l::set('to', 'bis');

l::set('contact', 'Kontakt');
l::set('pages', 'Seiten');
l::set('service', 'Service');
l::set('network', 'Netzwerk');
l::set('download', 'Download');

l::set('cancel', 'Abbrechen');

// Search
l::set('search', 'Suchen');
l::set('search-nothing-found', 'Die Suche ergab leider keine Treffer.');

// Uniform
l::set('uniform-filled-potty', 'Es wurde das Feld ausgefüllt, das leer bleiben sollte. Falls Sie kein Spam-Bot sind, versuchen Sie es bitte erneut ohne das Feld auszufüllen.');
l::set('uniform-fields-required', 'Bitte füllen Sie alle benötigten Felder aus.');
l::set('uniform-fields-not-valid', 'Einige Felder enthalten ungültige Daten.');

l::set('uniform-email-subject', 'Nachricht über das Formular');
l::set('uniform-email-success', 'Vielen Dank, das Formular wurde gesendet.');
l::set('uniform-email-error', 'Es ist ein Fehler beim Senden aufgetreten:');
l::set('uniform-email-copy', ''); // Kopie:

l::set('uniform-calc-plus', 'plus');

l::set('uniform-log-success', 'Der Logeintrag wurde erfolgreich angelegt.');
l::set('uniform-log-error', 'Beim Schreiben in die Log-Datei ist ein Fehler aufgetreten.');

l::set('uniform-login-error', 'Benutzername oder Passwort falsch.');
l::set('uniform-login-success', 'Login erfolgreich.');

l::set('uniform-webhook-success', 'Aufruf des Webhook erfolgreich.');
l::set('uniform-webhook-error', 'Beim Aufruf des Webhook ist ein Fehler aufgetreten: ');

l::set('uniform-email-select-error', 'Ungültiger Empfänger.');

// Contact
l::set('contact-form', 'Kontaktformular');
l::set('company', 'Firma / Organisation');
l::set('salutation', 'Anrede');
l::set('name', 'Vorname');
l::set('surname', 'Nachname');
l::set('phone', 'Telefon');
l::set('email-address', 'Email');
l::set('message', 'Mitteilung');
l::set('submit', 'Senden');
l::set('location', 'So finden Sie uns');

?>
