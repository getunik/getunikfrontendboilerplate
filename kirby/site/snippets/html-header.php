<!doctype html>
<?php header('Content-type: text/html; charset=utf-8'); ?>
<html lang="<?php echo $site->language()->code() ?>">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="format-detection" content="telephone=no">

        <link rel="canonical" href="<?php echo $page->url() ?>">
        <meta name="description" content="<?php echo html($site->description()) ?>">
        <meta name="keywords" content="<?php echo html($site->keywords()) ?>">
        <meta name="author" content="<?php echo html($site->author()) ?>">

        <!-- Facebook / using generic app_id -->
        <meta property="fb:app_id" content="966242223397117" />
        <meta property="og:locale" content="de_DE">
        <meta property="og:type" content="website">
        <meta property="og:title" content="<?php echo html($site->title()) ?>">
        <meta property="og:description" content="<?php echo html($site->description()) ?>">
        <meta property="og:url" content="<?php echo $page->url() ?>">
        <meta property="og:site_name" content="<?php echo html($site->title()) ?>">
        <meta property="og:image" content="<?php echo url('assets/images/meta/xxx.jpg') ?>">

        <!-- Twitter -->
        <meta name="twitter:card" content="summary">
        <meta name="twitter:description" content="<?php echo html($site->description()) ?>">
        <meta name="twitter:title" content="<?php echo html($site->title()) ?>">
        <meta name="twitter:domain" content="<?php echo $site->url() ?>">
        <meta name="twitter:image:src" content="<?php echo url('assets/images/meta/xxx.jpg') ?>">

        <!-- CSS -->
        <?php echo css('assets/css/styles.min.css') ?>

        <!-- Favicons -->
        <link rel="icon" href="<?php echo url('favicon.ico') ?>" />
        <link rel="apple-touch-icon" sizes="57x57" href="<?php echo url('assets/images/favicons/apple-icon-57x57.png') ?>">
        <link rel="apple-touch-icon" sizes="60x60" href="<?php echo url('assets/images/favicons/apple-icon-60x60.png') ?>">
        <link rel="apple-touch-icon" sizes="72x72" href="<?php echo url('assets/images/favicons/apple-icon-72x72.png') ?>">
        <link rel="apple-touch-icon" sizes="76x76" href="<?php echo url('assets/images/favicons/apple-icon-76x76.png') ?>">
        <link rel="apple-touch-icon" sizes="114x114" href="<?php echo url('assets/images/favicons/apple-icon-114x114.png') ?>">
        <link rel="apple-touch-icon" sizes="120x120" href="<?php echo url('assets/images/favicons/apple-icon-120x120.png') ?>">
        <link rel="apple-touch-icon" sizes="144x144" href="<?php echo url('assets/images/favicons/apple-icon-144x144.png') ?>">
        <link rel="apple-touch-icon" sizes="152x152" href="<?php echo url('assets/images/favicons/apple-icon-152x152.png') ?>">
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo url('assets/images/favicons/apple-icon-180x180.png') ?>">
        <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo url('assets/images/favicons/android-icon-192x192.png') ?>">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo url('assets/images/favicons/favicon-32x32.png') ?>">
        <link rel="icon" type="image/png" sizes="96x96" href="<?php echo url('assets/images/favicons/favicon-96x96.png') ?>">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo url('assets/images/favicons/favicon-16x16.png') ?>">
        <link rel="manifest" href="<?php echo url('assets/images/favicons/manifest.json') ?>">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="<?php echo url('assets/images/favicons/ms-icon-144x144.png') ?>">
        <meta name="theme-color" content="#ffffff">

        <title><?php echo html($page->main_title()) ?> | <?php echo html($site->title()) ?></title>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <?php echo kirbyjs($page); ?>
    </head>
    <body class="<?php echo $page->uid() ?> <?php echo 'env-' . ENVIRONMENT; ?> <?php echo (c::get('debug') ? 'dev' : ''); ?>">


	<?php snippet('menu/main-menu-first-level') ?>

