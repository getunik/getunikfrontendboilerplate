<div class="ui-footer">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-3">

				<h4><?php echo l::get('pages') ?></h4>

				<ul>
					<?php
					// main menu items
					$items = $pages->visible();
					// only show the menu if items are available
					if($items->count()):
						?>

						<?php foreach($items as $item): ?>
						<li<?php e($item->isOpen(), ' class="active"') ?>><a<?php e($item->isOpen(), ' class="active"') ?> href="<?php echo $item->url() ?>"><?php echo $item->title()->html() ?></a></li>
					<?php endforeach ?>

					<?php endif ?>
				</ul>

			</div><!-- /.col-xs-12 col-sm-3 -->

			<div class="col-xs-12 col-sm-3">

				<h4><?php echo l::get('language') ?></h4>

				<?php snippet('menu/lang-menu-switcher') ?>

			</div><!-- /.col-xs-12 col-sm-3 -->

		</div><!-- /.row -->

		<div class="row">
			<div class="col-xs-12">
				<div class="ui-copyright">
					<hr />
					<?php echo $site->copyright() ?>
				</div><!-- /.ui-copyright -->
			</div><!-- /.col-xs-12 -->
		</div><!-- /.row -->

	</div><!-- /.container -->
</div><!-- /.ui-footer -->
