Main-content:

----

Title: DE Basic page

----

Maintitle: DE Basic Page / Content page

----

Lead: DE Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.

----

Text:

DE Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.

##Pellentesque habitant

Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.

Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.

- DE One shoreditch
- cornhole
- locavore
- humblebrag
- chambray

1. DE One shoreditch
2. cornhole
3. locavore
4. humblebrag
5. chambray

----

Sidebar:

----

Sidebartitle: DE Sidebar

----

Sidebarcontent:

- DE One shoreditch
- cornhole
- locavore
- humblebrag
- chambray

1. DE One shoreditch
2. cornhole
3. locavore
4. humblebrag
5. chambray

----
