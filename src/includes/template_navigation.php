  <div class="container">

    <div class="row">
      <div class="col-xs-12">

		<div class="btn-group pull-right" role="group" aria-label="btn-group">
			<a href="/" class="btn btn-xs btn-default">Home</a>
			<a href="page-1.html" class="btn btn-xs btn-default">Page 1</a>
			<a href="page-2.html" class="btn btn-xs btn-default">Page 2</a>
			<a href="page-3.html" class="btn btn-xs btn-default">Page 3</a>
		</div> <!-- /.btn-group -->

      </div><!-- /.col -->
    </div><!-- /.row -->

  </div><!-- /.container -->
