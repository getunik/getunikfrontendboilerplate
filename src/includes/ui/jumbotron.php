
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="ui-heading-sub-no-border">
                Jumbotron
            </div><!-- /.ui-heading -->
        </div><!-- /.col-xs-12 -->
    </div><!-- /.row -->
</div><!-- /.container -->

<div class="getu-jumbotron">

    <div class="getu-jumbotron-image-wrapper" style="background-image: url(<?php echo $imagePath; ?>ui_keyvisual_1920.jpg)">

        <div class="getu-jumbotron-tint"></div>

        <div class="getu-jumbotron-image visible-xs">
            <img class="img-responsive" src="<?php echo $imagePath; ?>ui_keyvisual_portrait_720.jpg" alt="">
        </div>

        <div class="getu-jumbotron-title-wrapper">
            <h1 class="text-center">Fashion fixie quinoa</h1>
            <p class="lead text-center">Chartreuse 90's shoreditch gluten-free cornhole locavore kogi quinoa, cardigan tumblr mlkshk you probably semiotics. Kale chips photo booth PBR&B yuccie venmo, cardigan truffaut +1 health goth try-hard brunch. Health goth green juice jean shorts chambray fixie hashtag.</p><!-- /.lead -->

            <div class="getu-jumbotron-btn-wrapper text-center">
                <a href="" class="btn btn-lg btn-primary btn-primary-inverted">Quinoa</a>
            </div>
        </div>

    </div>

</div>



<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="ui-heading-sub-no-border">
                Jumbotron Image Apart
            </div><!-- /.ui-heading -->
        </div><!-- /.col-xs-12 -->
    </div><!-- /.row -->
</div><!-- /.container -->

<div class="getu-jumbotron getu-jumbotron-image-apart">

    <div class="getu-jumbotron-image-wrapper" style="background-image: url(<?php echo $imagePath; ?>ui_keyvisual_1920.jpg)">

        <div class="getu-jumbotron-tint"></div>

        <div class="getu-jumbotron-image visible-xs">
            <img class="img-responsive" src="<?php echo $imagePath; ?>ui_keyvisual_720.jpg" alt="">
        </div>

        <div class="getu-jumbotron-title-wrapper">
            <h1 class="text-center">Fashion fixie quinoa</h1>
            <p class="lead text-center">Chartreuse 90's shoreditch gluten-free cornhole locavore kogi quinoa, cardigan tumblr mlkshk you probably semiotics. Kale chips photo booth PBR&B yuccie venmo, cardigan truffaut +1 health goth try-hard brunch. Health goth green juice jean shorts chambray fixie hashtag.</p><!-- /.lead -->

            <div class="getu-jumbotron-btn-wrapper text-center">
                <a href="" class="btn btn-lg btn-primary btn-primary-inverted hidden-xs">Chartreuse</a>
                <a href="" class="btn btn-lg btn-primary btn-primary visible-xs">Chartreuse</a>
            </div>
        </div>

    </div>

</div><!-- /.p2p-jumbotron -->

