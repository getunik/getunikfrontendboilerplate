
<section id="alerts">

	<div class="ui-heading">Alerts</div><!-- /.ui-heading -->

	<div class="ui-heading-sub-no-border">Default</div><!-- /.ui-heading-sub -->

	<div class="alert alert-success" role="alert">Heads up! This alert needs your attention, but it's not super important.</div>
	<div class="alert alert-info" role="alert">Heads up! This alert needs your attention, but it's not super important.</div>
	<div class="alert alert-warning" role="alert">Heads up! This alert needs your attention, but it's not super important.</div>
	<div class="alert alert-danger" role="alert">Oh snap! Change a few things up and try submitting again.</div>

	<div class="ui-heading-sub-no-border">Dismissible Alerts</div><!-- /.ui-heading-sub -->

	<div class="alert alert-warning alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		Warning! Better check yourself, you're not looking too good.
	</div>

	<div class="alert alert-danger alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		Oh snap! Change a few things up and try submitting again.
	</div>

</section><!-- /#alerts -->
