
<section id="forms">

	<div class="ui-heading">Forms</div><!-- /.ui-heading -->

	<div class="ui-heading-sub">Input Fields</div><!-- /.ui-heading -->


	<div class="row">
		<div class="col-md-6">

			<div class="form-group">
				<input type="text" class="form-control input-lg" placeholder="Placeholder" />
			</div><!-- /.form-group -->

			<div class="form-group has-error">
				<input type="text" class="form-control input-lg" placeholder="Placeholder" value="Has Error ..."/>
			</div><!-- /.form-group -->

			<div class="form-group has-success">
				<input type="text" class="form-control input-lg" placeholder="Placeholder" value="Has Success ..."/>
			</div><!-- /.form-group -->

			<div class="form-group">
				<input type="text" class="form-control input-lg" placeholder="Placeholder" value="Disabled Input" disabled />
			</div><!-- /.form-group -->

			<div class="form-group">
				<label class="control-label" for="inputLabel">Input with Label</label>
				<input type="text" id="inputLabel" class="form-control input-lg" placeholder="Placeholder" />
			</div><!-- /.form-group -->

		</div><!-- /.col-md-6 -->

		<div class="col-md-6">

			<div class="form-group">
				<input type="text" class="form-control input-lg" placeholder="Placeholder" value="Sample ..."/>
			</div><!-- /.form-group -->

			<div class="form-group has-error has-feedback">
				<label class="control-label" for="inputError2">Input with error</label>
				<input type="text" class="form-control input-lg" id="inputError2" aria-describedby="inputError2Status" value="Has Error ...">
				<span class="glyphicon glyphicon-exclamation-sign form-control-feedback" aria-hidden="true"></span>
				<span id="inputError2Status" class="sr-only">(error)</span>
			</div><!-- /.form-group -->

			<div class="form-group has-success has-feedback">
				<label class="control-label" for="inputSuccess2">Input with success</label>
				<input type="text" class="form-control input-lg" id="inputSuccess2" aria-describedby="inputSuccess2Status" value="Has Success ...">
				<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>
				<span id="inputSuccess2Status" class="sr-only">(success)</span>
			</div><!-- /.form-group -->

			<div class="form-group has-error has-feedback">
				<label class="control-label" for="inputError2">Input with error</label>
				<input type="text" class="form-control input-lg" id="inputError2" aria-describedby="inputError2Status" value="Has Error ...">
				<span class="glyphicon glyphicon-exclamation-sign form-control-feedback" aria-hidden="true"></span>
				<span id="inputError2Status" class="sr-only">(error)</span>
				<div class="form-error">Ooops, looks like an error</div><!-- /.form-error -->
			</div><!-- /.form-group -->

		</div><!-- /.col-md-6 -->
	</div><!-- /.row -->




	<div class="ui-heading-sub">Selects selectize.js</div><!-- /.ui-heading -->

	<div class="row">
		<div class="col-md-6">

			<div class="form-group">
				<label class="control-label" for="">Select with label</label>
				<select class="form-control input-lg selectize" placeholder="Select a option ...">
					<option value="">Select a option ...</option>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
				</select>
			</div><!-- /.form-group -->

		</div><!-- /.col-md-6 -->

		<div class="col-md-6">

			<div class="form-group has-error">
				<label class="control-label" for="">Select with error</label>
				<select class="form-control input-lg selectize">
					<option value="1">Has Error</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
				</select>
				<div class="form-error">Ooops, looks like an error</div><!-- /.form-error -->
			</div><!-- /.form-group -->

		</div><!-- /.col-md-6 -->
	</div><!-- /.row -->


	<div class="ui-heading-sub">Selects (Browser Default)</div><!-- /.ui-heading -->

	<div class="row">
		<div class="col-md-6">

			<div class="form-group">
				<select class="form-control">
					<option>Default Browser Styled</option>
					<option>2</option>
					<option>3</option>
					<option>4</option>
					<option>5</option>
				</select>
			</div><!-- /.form-group -->

		</div><!-- /.col-md-6 -->

		<div class="col-md-6">

			<div class="form-group has-error">
				<select class="form-control">
					<option>Has Error</option>
					<option>2</option>
					<option>3</option>
					<option>4</option>
					<option>5</option>
				</select>
			</div><!-- /.form-group -->

		</div><!-- /.col-md-6 -->
	</div><!-- /.row -->



	<div class="row">
		<div class="col-md-6">
			<div class="ui-heading-sub">Checkboxes & Radio Buttons</div><!-- /.ui-heading -->

			<div class="checkbox">
				<input type="checkbox" class="getunik-checkbox" id="checkbox1" value="">
				<label for="checkbox1">Option one is this and that&mdash;be sure to include why it's great</label>
			</div>
			<div class="checkbox">
				<input type="checkbox" class="getunik-checkbox" value="" id="checkbox2">
				<label for="checkbox2">Option two can be something else and selecting</label>
			</div>
			<div class="checkbox disabled">
				<input type="checkbox" class="getunik-checkbox" value="" id="checkbox3" disabled>
				<label for="checkbox3">Option two is disabled</label>
			</div>

			<div class="radio">
				<input type="radio" class="getunik-radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
				<label for="optionsRadios1">Option one is this and that&mdash;be sure to include why it's great</label>
			</div>
			<div class="radio">
				<input type="radio" class="getunik-radio" name="optionsRadios" id="optionsRadios2" value="option2">
				<label for="optionsRadios2">Option two can be something else and selecting it will deselect option one</label>
			</div>
			<div class="radio disabled">
				<input type="radio" class="getunik-radio" name="optionsRadios" id="optionsRadios3" value="option3" disabled>
				<label for="optionsRadios3">Option three is disabled</label>
			</div>

		</div><!-- /.col-md-6 -->

		<div class="col-md-6">
			<div class="ui-heading-sub">Text Areas</div><!-- /.ui-heading -->

			<div class="form-group">
				<label for="textArea">Textarea with Label</label>
				<textarea class="form-control" name="" id="textArea" cols="30" rows="10" placeholder="Text eingeben ..." ></textarea>
			</div><!-- /.form-group -->
		</div><!-- /.col-md-6 -->
	</div><!-- /.row -->

</section><!-- /#forms -->


