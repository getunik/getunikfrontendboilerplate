

	<section id="typography" class="clearfix">

		<div class="ui-heading">Typography</div>

		<div class="ui-heading-sub">Headings</div>

		<h1>H1 Fashion axe fixie vegan quinoa</h1>

		<h2>H2 Fashion axe fixie vegan quinoa</h2>

		<h3>H3 Fashion axe fixie vegan quinoa</h3>

		<h4>H4 Fashion axe fixie vegan quinoa</h4>

		<h5>H5 Fashion axe fixie vegan quinoa</h5>

		<h6>H6 Fashion axe fixie vegan quinoa</h6>



		<div class="ui-heading-sub">Lead Copy</div>

		<p class="lead">Chartreuse 90's shoreditch gluten-free cornhole locavore kogi quinoa, cardigan tumblr mlkshk you probably haven't heard of them semiotics. Kale chips photo booth PBR&B yuccie venmo, cardigan truffaut +1 health goth try-hard brunch. Health goth green juice jean shorts chambray fixie hashtag. Sartorial literally helvetica, artisan seitan ethical fashion axe post-ironic chambray kitsch meditation deep v thundercats.</p>



		<div class="ui-heading-sub">Body Copy</div>

		<p>Chartreuse 90's shoreditch gluten-free cornhole locavore kogi quinoa, cardigan tumblr mlkshk you probably haven't heard of them semiotics. Kale chips photo booth PBR&B yuccie venmo, cardigan truffaut +1 health goth try-hard brunch. Health goth green juice jean shorts chambray fixie hashtag. Sartorial literally helvetica, artisan seitan ethical fashion axe post-ironic chambray kitsch meditation deep v thundercats.</p>
		<p>Neutra polaroid vice, try-hard venmo beard actually humblebrag tofu chambray health goth franzen you probably haven't heard of them 3 wolf moon paleo. Cliche bespoke normcore single-origin coffee put a bird on it, heirloom trust fund health goth gastropub kogi pinterest kinfolk umami ennui street art. Selfies fixie disrupt, tacos asymmetrical truffaut hoodie quinoa williamsburg small batch.</p>


		<div class="ui-heading-sub">Lists</div>

		<ul>
			<li>One shoreditch</li>
			<li>cornhole</li>
			<li>locavore</li>
			<li>humblebrag</li>
			<li>chambray</li>
		</ul>

		<ol>
			<li>One shoreditch</li>
			<li>cornhole</li>
			<li>locavore</li>
			<li>humblebrag</li>
			<li>chambray</li>
		</ol>


		<div class="ui-heading-sub">Teaser Copy</div>

		<p class="teaser">Chartreuse 90's shoreditch gluten-free cornhole locavore kogi quinoa, cardigan tumblr mlkshk you probably haven't heard of them semiotics. Kale chips photo booth PBR&B yuccie venmo, cardigan truffaut +1 health goth try-hard brunch. Health goth green juice jean shorts chambray fixie hashtag. Sartorial literally helvetica, artisan seitan ethical fashion axe post-ironic chambray kitsch meditation deep v thundercats.</p>
		<p class="teaser">Neutra polaroid vice, try-hard venmo beard actually humblebrag tofu chambray health goth franzen you probably haven't heard of them 3 wolf moon paleo. Cliche bespoke normcore single-origin coffee put a bird on it, heirloom trust fund health goth gastropub kogi pinterest kinfolk umami ennui street art. Selfies fixie disrupt, tacos asymmetrical truffaut hoodie quinoa williamsburg small batch.</p>



		<div class="ui-heading-sub">Links</div>

		<p><a href="">Clickable Link</a></p>



		<div class="ui-heading-sub">Quotes</div>

		<blockquote>
			<p>Chartreuse 90's shoreditch gluten-free cornhole locavore kogi quinoa, cardigan tumblr mlkshk you probably haven't heard of them semiotics. Kale chips photo booth PBR&B yuccie venmo.</p>
		</blockquote>

		<blockquote>
			<div class="media">
				<div class="media-left">
					<a href="#">
						<img class="media-object getu-img-round getu-img-128" alt="" src="<?php echo $imagePath; ?>/ui_profile_256.jpg">
					</a>
				</div>
				<div class="media-body">
					<p>Chartreuse 90's shoreditch gluten-free cornhole locavore kogi quinoa, cardigan tumblr mlkshk you probably haven't heard of them semiotics. Kale chips photo booth PBR&B yuccie venmo.</p>
					<footer>Brandon Guidelines</footer>
				</div>
			</div>
		</blockquote>

	</section><!-- /#typography -->
