

<section id="tables">

	<div class="ui-heading">
		Tables
	</div><!-- /.ui-heading -->

	<div class="ui-heading-sub-no-border">
		Default table
	</div><!-- /.ui-heading-sub-no-border -->

	<table class="table">
		<thead>
			<tr>
				<th>Amount</th>
				<th>Card</th>
				<th>Card Holder</th>
				<th>Status</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>CHF 100</td>
				<td>Mastercard</td>
				<td>Mal Function</td>
				<td><span class="label label-primary">Primary</span></td>
			</tr>
			<tr>
				<td>CHF 115</td>
				<td>Visa</td>
				<td>Sue Shei</td>
				<td><span class="label label-default">Primary</span></td>
			</tr>
			<tr>
				<td>CHF 100</td>
				<td>Mastercard</td>
				<td>Brandon Guidelines</td>
				<td><span class="label label-primary">Primary</span></td>
			</tr>
			<tr>
				<td>CHF 115</td>
				<td>Mastercard</td>
				<td>Spruce Springclean</td>
				<td><span class="label label-default">Primary</span></td>
			</tr>
		</tbody>
	</table>



	<div class="ui-heading-sub-no-border">
		Striped table inside panel
	</div><!-- /.ui-heading-sub-no-border -->



	<div class="panel panel-default">
		<div class="panel-body">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Amount</th>
						<th>Card</th>
						<th>Card Holder</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>CHF 100</td>
						<td>Mastercard</td>
						<td>Mal Function</td>
						<td><span class="label label-primary">Primary</span></td>
					</tr>
					<tr>
						<td>CHF 115</td>
						<td>Visa</td>
						<td>Sue Shei</td>
						<td><span class="label label-default">Primary</span></td>
					</tr>
					<tr>
						<td>CHF 100</td>
						<td>Mastercard</td>
						<td>Brandon Guidelines</td>
						<td><span class="label label-primary">Primary</span></td>
					</tr>
					<tr>
						<td>CHF 115</td>
						<td>Mastercard</td>
						<td>Spruce Springclean</td>
						<td><span class="label label-default">Primary</span></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

</section><!-- /#tables -->
