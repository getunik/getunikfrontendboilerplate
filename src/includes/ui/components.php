

<section id="components">
	<div class="ui-heading">Components</div><!-- /.ui-heading -->

	<div class="ui-heading-sub-no-border">Progress Bars</div><!-- /.ui-heading-sub-no-border -->

	<div class="progress">
		<div class="progress-bar" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
			<span class="sr-only">40% Complete (success)</span>
		</div>
	</div>
	<div class="progress">
		<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
			<span class="sr-only">20% Complete</span>
		</div>
	</div>



	<div class="row">
		<div class="col-md-6">
			<div class="ui-heading-sub-no-border">Labels</div><!-- /.ui-heading-sub-no-border -->

			<span class="label label-primary">Primary</span>
			<span class="label label-default">Default</span>
		</div><!-- /.col-md-6 -->

		<div class="col-md-6">
			<div class="ui-heading-sub-no-border">Badges</div><!-- /.ui-heading-sub-no-border -->

			<span class="badge">42</span>
		</div><!-- /.col-md-6 -->
	</div>



	<div class="row">
		<div class="col-md-6">
			<div class="ui-heading-sub-no-border">Pagers</div><!-- /.ui-heading-sub-no-border -->

			<nav>
				<ul class="pager">
					<li><a href="#">Previous</a></li>
					<li><a href="#">Next</a></li>
				</ul>
			</nav>

			<nav>
				<ul class="pager">
					<li class="previous"><a href="#"><span aria-hidden="true">&larr;</span> Older</a></li>
					<li class="next"><a href="#">Newer <span aria-hidden="true">&rarr;</span></a></li>
				</ul>
			</nav>

			<nav>
				<ul class="pager">
					<li class="previous disabled"><a href="#"><span aria-hidden="true">&larr;</span> Older</a></li>
					<li class="next disabled"><a href="#">Newer <span aria-hidden="true">&rarr;</span></a></li>
				</ul>
			</nav>

		</div><!-- /.col-md-6 -->
		<div class="col-md-6">
			<div class="ui-heading-sub-no-border">Pagination</div><!-- /.ui-heading-sub-no-border -->

			<nav>
				<ul class="pagination">
					<li>
						<a href="#" aria-label="Previous">
							<span aria-hidden="true">&laquo;</span>
						</a>
					</li>
					<li><a href="#">1</a></li>
					<li><a href="#">2</a></li>
					<li class="active"><a href="#">3</a></li>
					<li><a href="#">4</a></li>
					<li><a href="#">5</a></li>
					<li><a href="#">6</a></li>
					<li><a href="#">7</a></li>
					<li><a href="#">8</a></li>
					<li><a href="#">9</a></li>
					<li><a href="#">10</a></li>
					<li>
						<a href="#" aria-label="Next">
							<span aria-hidden="true">&raquo;</span>
						</a>
					</li>
				</ul>
			</nav>
		</div><!-- /.col-md-6 -->
	</div><!-- /.row -->



	<div class="row">
		<div class="col-md-6">
			<div class="ui-heading-sub-no-border">Tooltip</div><!-- /.ui-heading-sub-no-border -->

			<button type="button" class="btn btn-primary btn-lg" data-toggle="tooltip" data-placement="top" title="Tooltip on top">Tooltip on top</button>
		</div><!-- /.col-md-6 -->
		<div class="col-md-6">

		</div><!-- /.col-md-6 -->
	</div><!-- /.row -->


</section><!-- /#components -->

