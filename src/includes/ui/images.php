<section id="images">

	<div class="ui-heading">
		Images
	</div><!-- /.ui-heading -->

	<div class="ui-heading-sub-no-border">

	</div><!-- /.ui-heading -->

	<a class="getu-img-link getu-img-link-256" href="">
		<img class="getu-img-round getu-img-256" src="<?php echo $imagePath; ?>/ui_profile_256.jpg" alt="" />
	</a>

	<a class="getu-img-link getu-img-link-128" href="">
		<img class="getu-img-round getu-img-128" src="<?php echo $imagePath; ?>/ui_profile_256.jpg" alt="" />
	</a>

	<a class="getu-img-link getu-img-link-64" href="">
		<img class="getu-img-round getu-img-64" src="<?php echo $imagePath; ?>/ui_profile_256.jpg" alt="" />
	</a>

	<a class="getu-img-link getu-img-link-32" href="">
		<img class="getu-img-round getu-img-32" src="<?php echo $imagePath; ?>/ui_profile_256.jpg" alt="" />
	</a>

</section><!-- /#images -->
