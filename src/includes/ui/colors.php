

<section id="colors">
	<div class="ui-heading">
		Colors
	</div><!-- /.ui-heading -->


	<div class="ui-heading-sub-no-border">
		Primaries
	</div><!-- /.ui-heading-sub-no-border -->

	<div class="ui-color ui-color-primary">@brand-primary</div><!-- /.ui-color-primary -->
	<div class="ui-color ui-color-secondary">@brand-secondary</div><!-- /.ui-color-secondary -->

	<div class="ui-heading-sub-no-border">
		Grayscales
	</div><!-- /.ui-heading-sub-no-border -->

	<div class="ui-color ui-color-gray-1">@getu-gray-1</div><!-- /.ui-color-gray-1 -->
	<div class="ui-color ui-color-gray-2">@getu-gray-2</div><!-- /.ui-color-gray-1 -->
	<div class="ui-color ui-color-gray-3">@getu-gray-3</div><!-- /.ui-color-gray-1 -->
	<div class="ui-color ui-color-gray-4">@getu-gray-4</div><!-- /.ui-color-gray-1 -->

</section><!-- /#colors -->
