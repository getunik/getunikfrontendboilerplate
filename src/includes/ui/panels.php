


<section id="panels">
	<div class="ui-heading">Panels</div><!-- /.ui-heading -->

	<div class="ui-heading-sub-no-border">
		Panel without heading
	</div><!-- /.ui-heading-sub-no-border -->

	<div class="panel panel-default">
	  <div class="panel-body">
	    Fashion axe fixie vegan quinoa ...
	  </div>
	</div>

	<div class="ui-heading-sub-no-border">
		Panel with heading and title
	</div><!-- /.ui-heading-sub-no-border -->

	<div class="panel panel-default">
	  <div class="panel-heading">
	    <h3 class="panel-title">Panel title</h3>
	  </div>
	  <div class="panel-body">
	    Fashion axe fixie vegan quinoa ...
	  </div>
	</div>
</section><!-- /#panels -->
