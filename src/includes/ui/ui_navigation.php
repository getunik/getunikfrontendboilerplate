<?php

	if (!isset($variables['activeStep'])) {
		$variables['activeStep'] = false;
	}

	$navigation = array(
		'index.html' => 'GUIDE_BS',
		'ui_design_pattern.html' => 'UI Design Pattern',
		'ui_components.html' => 'Components',
		'ui_landing_page.html' => 'Landing Page',
		'ui_basic_page.html' => 'Basic Page'
	);

	$i = 0;
	foreach ($navigation as $key=>$value) {
		$class = ($i === $variables['activeStep']) ? 'active' : '';
		echo '<li class="' . $class . '"><a href="' . $key . '" >' . $value . '</a></li>';
		$i++;
	}

 ?>
