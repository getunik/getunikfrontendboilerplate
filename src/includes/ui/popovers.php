

<section id="popovers">
	<div class="ui-heading">
		Popovers
	</div><!-- /.ui-heading -->

	<br/>

	<button type="button" class="btn btn-default btn-lg" data-container="body" data-toggle="popover" data-placement="left" title="Popover title" data-content="Chartreuse 90's shoreditch gluten-free cornhole locavore kogi quinoa, cardigan tumblr mlkshk you probably haven't heard of them semiotics. Kale chips photo booth PBR&B yuccie venmo.">
		Popover on left
	</button>

	<button type="button" class="btn btn-default btn-lg" data-container="body" data-toggle="popover" data-placement="top" title="Popover title" data-content="Chartreuse 90's shoreditch gluten-free cornhole locavore kogi quinoa, cardigan tumblr mlkshk you probably haven't heard of them semiotics. Kale chips photo booth PBR&B yuccie venmo.">
		Popover on top
	</button>

	<button type="button" class="btn btn-default btn-lg" data-container="body" data-toggle="popover" data-placement="bottom" title="Popover title" data-content="Chartreuse 90's shoreditch gluten-free cornhole locavore kogi quinoa, cardigan tumblr mlkshk you probably haven't heard of them semiotics. Kale chips photo booth PBR&B yuccie venmo.">
		Popover on bottom
	</button>

	<button type="button" class="btn btn-default btn-lg" data-container="body" data-toggle="popover" data-placement="right" title="Popover title" data-content="Chartreuse 90's shoreditch gluten-free cornhole locavore kogi quinoa, cardigan tumblr mlkshk you probably haven't heard of them semiotics. Kale chips photo booth PBR&B yuccie venmo.">
		Popover on right
	</button>
</section><!-- /#popovers -->

