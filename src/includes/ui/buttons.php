
<section id="buttons">

	<div class="ui-heading">Buttons</div>


	<div class="row">
		<div class="col-md-6">
			<div class="ui-heading-sub-no-border">Button Primary Large</div>

			<a href="" class="btn btn-primary btn-block btn-lg">Button</a>
			<a href="" class="btn btn-primary btn-block btn-lg disabled">Button disabled</a>

		</div><!-- /.col-md-6 -->

		<div class="col-md-6">

			<div class="ui-heading-sub-no-border">Button Default Large</div>

			<a href="" class="btn btn-default btn-block btn-lg">Button</a>
			<a href="" class="btn btn-default btn-block btn-lg disabled">Button disabled</a>

		</div><!-- /.col-md-6 -->
	</div><!-- /.row -->



	<div class="row">
		<div class="col-md-6">
			<div class="ui-heading-sub-no-border">Button Primary</div>

			<a href="" class="btn btn-primary btn-block btn-md">Button</a>
			<a href="" class="btn btn-primary btn-block btn-md disabled">Button disabled</a>
		</div><!-- /.col-md-6 -->

		<div class="col-md-6">
			<div class="ui-heading-sub-no-border">Button Default</div>

			<a href="" class="btn btn-default btn-block btn-md">Button</a>
			<a href="" class="btn btn-default btn-block btn-md disabled">Button disabled</a>
		</div><!-- /.col-md-6 -->
	</div><!-- /.row -->



	<div class="row">
		<div class="col-md-6">
			<div class="ui-heading-sub-no-border">Dynamic Primaries</div>

			<div class="row">
				<div class="col-xs-12">
					<a href="" class="btn btn-primary btn-lg">Button</a>
					<a href="" class="btn btn-primary btn-lg disabled">Button disabled</a>
				</div><!-- /.col-xs-12 -->

				<div class="col-xs-12">
					<a href="" class="btn btn-primary btn-md">Button</a>
					<a href="" class="btn btn-primary btn-md disabled">Button disabled</a>
				</div><!-- /.col-xs-12 -->
			</div><!-- /.row -->
		</div><!-- /.col-md-6 -->

		<div class="col-md-6">
			<div class="ui-heading-sub-no-border">Dynamic Defaults</div>

			<div class="row">
				<div class="col-xs-12">
					<a href="" class="btn btn-default btn-lg">Button</a>
					<a href="" class="btn btn-default btn-lg disabled">Button disabled</a>
				</div><!-- /.col-xs-12 -->

				<div class="col-xs-12">
					<a href="" class="btn btn-default btn-md">Button</a>
					<a href="" class="btn btn-default btn-md disabled">Button disabled</a>
				</div><!-- /.col-xs-12 -->
			</div><!-- /.row -->
		</div><!-- /.col-md-6 -->
	</div><!-- /.row -->





	<div class="ui-heading-sub-no-border">Use on dark backgrounds</div>



	<div class="row">
		<div class="col-md-6 ui-dark">

			<div class="">
				<div class="ui-heading-sub-no-border ui-heading-inverted">Button Primary Large</div>

				<p>
					<a href="" class="btn btn-primary btn-primary-inverted btn-block btn-lg">Button</a>
					<a href="" class="btn btn-primary btn-primary-inverted btn-block btn-lg disabled">Button disabled</a>
				</p>
			</div>

		</div><!-- /.col-md-6 -->

		<div class="col-md-6 ui-dark">

			<div class="">
				<div class="ui-heading-sub-no-border ui-heading-inverted">Button Default Large</div>

				<p>
					<a href="" class="btn btn-default btn-default-inverted btn-block btn-lg">Button</a>
					<a href="" class="btn btn-default btn-default-inverted btn-block btn-lg disabled">Button disabled</a>
				</p>
			</div><!-- /.col-xs-12 -->

		</div><!-- /.col-md-6 -->
	</div><!-- /.row -->




</section><!-- /#buttons -->

