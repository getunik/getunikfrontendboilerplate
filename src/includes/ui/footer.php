
<div class="ui-footer">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-3">

				<h4>Navigation</h4>

				<ul>
					<?php include('src/includes/ui/ui_navigation.php'); ?>
				</ul>

			</div><!-- /.col-xs-12 col-sm-3 -->
		</div><!-- /.row -->

		<div class="row">
			<div class="col-xs-12">
				<div class="ui-copyright">
					<hr />
					&copy; by <a href="http://getunik.com" target="_blank">getunik.com</a>
				</div><!-- /.ui-copyright -->
			</div><!-- /.col-xs-12 -->
		</div><!-- /.row -->

	</div><!-- /.container -->
</div><!-- /.ui-footer -->
