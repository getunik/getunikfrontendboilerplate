
<?php include('src/includes/helpers.php'); ?>
<?php include('src/includes/html/html_header.php'); ?>

<body class="dev">

  <?php includeFile('src/includes/ui/top_navi.php', array('activeStep' => 1)); ?>

  <div class="container">
    <div class="row">
      <div class="col-md-8">

        <h1>UI Design Pattern</h1>

        <?php include('src/includes/ui/typography.php'); ?>
        <?php include('src/includes/ui/buttons.php'); ?>
        <?php include('src/includes/ui/navs.php'); ?>
        <?php include('src/includes/ui/forms.php'); ?>
        <?php include('src/includes/ui/alerts.php'); ?>
        <?php include('src/includes/ui/components.php'); ?>
        <?php include('src/includes/ui/panels.php'); ?>
        <?php include('src/includes/ui/modals.php'); ?>
        <?php include('src/includes/ui/popovers.php'); ?>
        <?php include('src/includes/ui/tables.php'); ?>
        <?php include('src/includes/ui/colors.php'); ?>
        <?php include('src/includes/ui/images.php'); ?>

      </div><!-- /.col-md-8 -->
      <nav class="col-md-3 col-md-offset-1 ui-sidebar-navigation hidden-sm hidden-xs">

        <ul class="nav nav-stacked">
          <li><a href="#typography">Typography</a></li>
          <li><a href="#buttons">Buttons</a></li>
          <li><a href="#navs">Navigations</a></li>
          <li><a href="#forms">Forms</a></li>
          <li><a href="#alerts">Alerts</a></li>
          <li><a href="#components">Components</a></li>
          <li><a href="#panels">Panels</a></li>
          <li><a href="#modals">Modals</a></li>
          <li><a href="#popovers">Popovers</a></li>
          <li><a href="#tables">Tables</a></li>
          <li><a href="#colors">Colors</a></li>
          <li><a href="#images">Images</a></li>
        </ul>

      </nav><!-- /.col-md-3 -->

    </div><!-- /.row -->
  </div><!-- /.container -->



  <?php include('src/includes/ui/footer.php'); ?>

  <?php include('src/includes/html/html_footer.php'); ?>
