
  <?php include('src/includes/helpers.php'); ?>
  <?php include('src/includes/html/html_header.php'); ?>

  <body class="dev">

      <?php includeFile('src/includes/ui/top_navi.php', array('activeStep' => 2)); ?>

      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <h1>Components</h1>
          </div><!-- /.col-xs-12 -->
        </div><!-- /.row -->
      </div><!-- /.container -->


      <?php include('src/includes/ui/jumbotron.php'); ?>
      <?php include('src/includes/ui/carousel.php'); ?>
      <?php include('src/includes/ui/footer.php'); ?>

  <?php include('src/includes/html/html_footer.php'); ?>
