
  <?php include('src/includes/helpers.php'); ?>
  <?php include('src/includes/html/html_header.php'); ?>

  <body class="dev">

      <?php includeFile('src/includes/ui/top_navi.php', array('activeStep' => 3)); ?>

      <div class="getu-carousel">

        <div class="getu-jumbotron getu-jumbotron-image-apart">

          <div class="getu-jumbotron-image-wrapper" style="background-image: url(<?php echo $imagePath; ?>ui_keyvisual_1920.jpg)">

            <div class="getu-jumbotron-tint"></div>

            <div class="getu-jumbotron-image visible-xs">
              <img class="img-responsive" src="<?php echo $imagePath; ?>ui_keyvisual_720.jpg" alt="">
            </div>

            <div class="getu-jumbotron-title-wrapper">
              <h1 class="text-center">Fashion fixie quinoa</h1>
              <p class="lead text-center">Chartreuse 90's shoreditch gluten-free cornhole locavore kogi quinoa, cardigan tumblr mlkshk you probably semiotics. Kale chips photo booth PBR&B yuccie venmo, cardigan truffaut +1 health goth try-hard brunch. Health goth green juice jean shorts chambray fixie hashtag.</p><!-- /.lead -->

              <div class="getu-jumbotron-btn-wrapper text-center">
                <a href="" class="btn btn-lg btn-primary btn-primary-inverted hidden-xs">Chartreuse</a>
                <a href="" class="btn btn-lg btn-primary btn-primary visible-xs">Chartreuse</a>
              </div>
            </div>

          </div>

        </div><!-- /.p2p-jumbotron -->

        <div class="getu-jumbotron getu-jumbotron-image-apart">

          <div class="getu-jumbotron-image-wrapper" style="background-image: url(<?php echo $imagePath; ?>ui_keyvisual_1920.jpg)">

            <div class="getu-jumbotron-tint"></div>

            <div class="getu-jumbotron-image visible-xs">
              <img class="img-responsive" src="<?php echo $imagePath; ?>ui_keyvisual_720.jpg" alt="">
            </div>

            <div class="getu-jumbotron-title-wrapper">
              <h1 class="text-center">Fashion fixie quinoa</h1>
              <p class="lead text-center">Chartreuse 90's shoreditch gluten-free cornhole locavore kogi quinoa, cardigan tumblr mlkshk you probably semiotics. Kale chips photo booth PBR&B yuccie venmo, cardigan truffaut +1 health goth try-hard brunch. Health goth green juice jean shorts chambray fixie hashtag.</p><!-- /.lead -->

              <div class="getu-jumbotron-btn-wrapper text-center">
                <a href="" class="btn btn-lg btn-primary btn-primary-inverted hidden-xs">Chartreuse</a>
                <a href="" class="btn btn-lg btn-primary btn-primary visible-xs">Chartreuse</a>
              </div>
            </div>

          </div>

        </div><!-- /.p2p-jumbotron -->

        <div class="getu-jumbotron getu-jumbotron-image-apart">

          <div class="getu-jumbotron-image-wrapper" style="background-image: url(<?php echo $imagePath; ?>ui_keyvisual_1920.jpg)">

            <div class="getu-jumbotron-tint"></div>

            <div class="getu-jumbotron-image visible-xs">
              <img class="img-responsive" src="<?php echo $imagePath; ?>ui_keyvisual_720.jpg" alt="">
            </div>

            <div class="getu-jumbotron-title-wrapper">
              <h1 class="text-center">Fashion fixie quinoa</h1>
              <p class="lead text-center">Chartreuse 90's shoreditch gluten-free cornhole locavore kogi quinoa, cardigan tumblr mlkshk you probably semiotics. Kale chips photo booth PBR&B yuccie venmo, cardigan truffaut +1 health goth try-hard brunch. Health goth green juice jean shorts chambray fixie hashtag.</p><!-- /.lead -->

              <div class="getu-jumbotron-btn-wrapper text-center">
                <a href="" class="btn btn-lg btn-primary btn-primary-inverted hidden-xs">Chartreuse</a>
                <a href="" class="btn btn-lg btn-primary btn-primary visible-xs">Chartreuse</a>
              </div>
            </div>

          </div>

        </div><!-- /.p2p-jumbotron -->

      </div><!-- /.getu-carousel -->


      <div class="container section">
        <div class="row">
          <div class="col-xs-12 text-center">
            <h2>Fashion fixie vegan quinoa</h2>
            <p class="lead">Chartreuse 90's shoreditch gluten-free cornhole locavore kogi quinoa, cardigan tumblr mlkshk you probably semiotics. Kale chips photo booth PBR&B yuccie venmo, cardigan truffaut +1 health goth try-hard brunch. Health goth green juice jean shorts chambray fixie hashtag. Sartorial literally helvetica..</p>


            <div class="col-md-4 col-md-offset-4">
              <button class="btn btn-primary btn-block">Button</button>
            </div><!-- /.col-md-4 -->


          </div><!-- /.col-xs-12 -->
        </div><!-- /.row -->
      </div><!-- /.container -->


      <div class="section dark text-center">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <h2>Fashion fixie vegan quinoa</h2>
            </div><!-- /.col-xs-12 -->
          </div><!-- /.row -->



          <div class="row">


            <div class="col-xs-12 col-sm-6 col-md-4">

              <div class="panel">
                <img src="<?php echo $imagePath ?>/ui_keyvisual_720.jpg" class="img-responsive" alt="" />
                <div class="panel-body teaser">
                  <h3>Fashion fixie vegan quinoa</h3>
                  <p>Chartreuse 90's shoreditch gluten-free cornhole locavore kogi quinoa, cardigan tumblr mlkshk you probably haven't heard of them semiotics.</p>
                  <a href="">Read more</a>
                </div><!-- /.panel-body -->
              </div><!-- /.panel -->

            </div><!-- /.col-xs-12 -->
            <div class="col-xs-12 col-sm-6 col-md-4">

              <div class="panel">
                <img src="<?php echo $imagePath ?>/ui_keyvisual_720.jpg" class="img-responsive" alt="" />
                <div class="panel-body teaser">
                  <h3>Fashion fixie vegan quinoa</h3>
                  <p>Chartreuse 90's shoreditch gluten-free cornhole locavore kogi quinoa, cardigan tumblr mlkshk you probably haven't heard of them semiotics.</p>
                  <a href="">Read more</a>
                </div><!-- /.panel-body -->
              </div><!-- /.panel -->

            </div><!-- /.col-xs-12 -->
            <div class="col-xs-12 col-sm-6 col-md-4">
              <div class="panel">
                <img src="<?php echo $imagePath ?>/ui_keyvisual_720.jpg" class="img-responsive" alt="" />
                <div class="panel-body teaser">
                  <h3>Fashion fixie vegan quinoa</h3>
                  <p>Chartreuse 90's shoreditch gluten-free cornhole locavore kogi quinoa, cardigan tumblr mlkshk you probably haven't heard of them semiotics.</p>
                  <a href="">Read more</a>
                </div><!-- /.panel-body -->
              </div><!-- /.panel -->

            </div><!-- /.col-xs-12 -->

            <div class="col-xs-12 col-sm-6 col-md-4">

              <div class="panel">
                <div class="panel-body teaser">
                  <img class="getu-img-round getu-img-128" src="<?php echo $imagePath ?>/ui_profile_256.jpg" alt="" />
                  <h3>Fashion fixie vegan quinoa</h3>
                  <p>Chartreuse 90's shoreditch gluten-free cornhole locavore kogi quinoa, cardigan tumblr mlkshk you probably haven't heard of them semiotics.</p>
                  <a class="btn btn-primary btn-block" href="">Button</a>
                </div><!-- /.panel-body -->
              </div><!-- /.panel -->

            </div><!-- /.col-xs-12 -->
            <div class="col-xs-12 col-sm-6 col-md-4">

              <div class="panel">
                <div class="panel-body teaser">
                  <img class="getu-img-round getu-img-128" src="<?php echo $imagePath ?>/ui_profile_256.jpg" alt="" />
                  <h3>Fashion fixie vegan quinoa</h3>
                  <p>Chartreuse 90's shoreditch gluten-free cornhole locavore kogi quinoa, cardigan tumblr mlkshk you probably haven't heard of them semiotics.</p>
                  <a class="btn btn-primary btn-block" href="">Button</a>
                </div><!-- /.panel-body -->
              </div><!-- /.panel -->

            </div><!-- /.col-xs-12 -->
            <div class="col-xs-12 col-sm-6 col-md-4">

              <div class="panel">
                <div class="panel-body teaser">
                  <img class="getu-img-round getu-img-128" src="<?php echo $imagePath ?>/ui_profile_256.jpg" alt="" />
                  <h3>Fashion fixie vegan quinoa</h3>
                  <p>Chartreuse 90's shoreditch gluten-free cornhole locavore kogi quinoa, cardigan tumblr mlkshk you probably haven't heard of them semiotics.</p>
                  <a class="btn btn-primary btn-block" href="">Button</a>
                </div><!-- /.panel-body -->
              </div><!-- /.panel -->

            </div><!-- /.col-xs-12 -->

          </div><!-- /.row -->

          <div class="row">

            <div class="col-md-4 col-md-offset-4">
              <button class="btn btn-default btn-block">Load more</button>
            </div><!-- /.col-md-4 -->

          </div><!-- /.row -->

        </div><!-- /.container -->
      </div><!-- /.dark -->



      <div class="getu-carousel">

        <div class="getu-jumbotron getu-jumbotron-image-apart">

          <div class="getu-jumbotron-image-wrapper" style="background-image: url(<?php echo $imagePath; ?>ui_keyvisual_1920.jpg)">

            <div class="getu-jumbotron-tint"></div>

            <div class="getu-jumbotron-image visible-xs">
              <img class="img-responsive" src="<?php echo $imagePath; ?>ui_keyvisual_720.jpg" alt="">
            </div>

            <div class="getu-jumbotron-title-wrapper">
              <h1 class="text-center">Fashion fixie quinoa</h1>
              <p class="lead text-center">Chartreuse 90's shoreditch gluten-free cornhole locavore kogi quinoa, cardigan tumblr mlkshk you probably semiotics. Kale chips photo booth PBR&B yuccie venmo, cardigan truffaut +1 health goth try-hard brunch. Health goth green juice jean shorts chambray fixie hashtag.</p><!-- /.lead -->

              <div class="getu-jumbotron-btn-wrapper text-center">
                <a href="" class="btn btn-lg btn-primary btn-primary-inverted hidden-xs">Chartreuse</a>
                <a href="" class="btn btn-lg btn-primary btn-primary visible-xs">Chartreuse</a>
              </div>
            </div>

          </div>

        </div><!-- /.p2p-jumbotron -->

        <div class="getu-jumbotron getu-jumbotron-image-apart">

          <div class="getu-jumbotron-image-wrapper" style="background-image: url(<?php echo $imagePath; ?>ui_keyvisual_1920.jpg)">

            <div class="getu-jumbotron-tint"></div>

            <div class="getu-jumbotron-image visible-xs">
              <img class="img-responsive" src="<?php echo $imagePath; ?>ui_keyvisual_720.jpg" alt="">
            </div>

            <div class="getu-jumbotron-title-wrapper">
              <h1 class="text-center">Fashion fixie quinoa</h1>
              <p class="lead text-center">Chartreuse 90's shoreditch gluten-free cornhole locavore kogi quinoa, cardigan tumblr mlkshk you probably semiotics. Kale chips photo booth PBR&B yuccie venmo, cardigan truffaut +1 health goth try-hard brunch. Health goth green juice jean shorts chambray fixie hashtag.</p><!-- /.lead -->

              <div class="getu-jumbotron-btn-wrapper text-center">
                <a href="" class="btn btn-lg btn-primary btn-primary-inverted hidden-xs">Chartreuse</a>
                <a href="" class="btn btn-lg btn-primary btn-primary visible-xs">Chartreuse</a>
              </div>
            </div>

          </div>

        </div><!-- /.p2p-jumbotron -->

        <div class="getu-jumbotron getu-jumbotron-image-apart">

          <div class="getu-jumbotron-image-wrapper" style="background-image: url(<?php echo $imagePath; ?>ui_keyvisual_1920.jpg)">

            <div class="getu-jumbotron-tint"></div>

            <div class="getu-jumbotron-image visible-xs">
              <img class="img-responsive" src="<?php echo $imagePath; ?>ui_keyvisual_720.jpg" alt="">
            </div>

            <div class="getu-jumbotron-title-wrapper">
              <h1 class="text-center">Fashion fixie quinoa</h1>
              <p class="lead text-center">Chartreuse 90's shoreditch gluten-free cornhole locavore kogi quinoa, cardigan tumblr mlkshk you probably semiotics. Kale chips photo booth PBR&B yuccie venmo, cardigan truffaut +1 health goth try-hard brunch. Health goth green juice jean shorts chambray fixie hashtag.</p><!-- /.lead -->

              <div class="getu-jumbotron-btn-wrapper text-center">
                <a href="" class="btn btn-lg btn-primary btn-primary-inverted hidden-xs">Chartreuse</a>
                <a href="" class="btn btn-lg btn-primary btn-primary visible-xs">Chartreuse</a>
              </div>
            </div>

          </div>

        </div><!-- /.p2p-jumbotron -->

      </div><!-- /.getu-carousel -->



      <div class="section container">
        <div class="row">

          <div class="col-xs-12 col-sm-6 col-md-4 text-center teaser">
            <img src="<?php echo $imagePath ?>/ui_keyvisual_720.jpg" class="img-responsive" alt="" />
            <h3>Fashion fixie vegan quinoa</h3>
            <p>Chartreuse 90's shoreditch gluten-free cornhole locavore kogi quinoa, cardigan tumblr mlkshk you probably haven't heard of them semiotics.</p>
          </div><!-- /.col-xs-12 col-sm-6 col-md-4 -->


          <div class="col-xs-12 col-sm-6 col-md-4 text-center teaser">
            <img src="<?php echo $imagePath ?>/ui_keyvisual_720.jpg" class="img-responsive" alt="" />
            <h3>Fashion fixie vegan quinoa</h3>
            <p>Chartreuse 90's shoreditch gluten-free cornhole locavore kogi quinoa, cardigan tumblr mlkshk you probably haven't heard of them semiotics.</p>
          </div><!-- /.col-xs-12 col-sm-6 col-md-4 -->

          <div class="col-xs-12 col-sm-6 col-md-4 text-center teaser">
            <img src="<?php echo $imagePath ?>/ui_keyvisual_720.jpg" class="img-responsive" alt="" />
            <h3>Fashion fixie vegan quinoa</h3>
            <p>Chartreuse 90's shoreditch gluten-free cornhole locavore kogi quinoa, cardigan tumblr mlkshk you probably haven't heard of them semiotics.</p>
          </div><!-- /.col-xs-12 col-sm-6 col-md-4 -->


        </div><!-- /.row -->


        <div class="row">

          <div class="col-xs-12 col-sm-6 col-md-6 teaser">
            <img src="<?php echo $imagePath ?>/ui_keyvisual_720.jpg" class="img-responsive" alt="" />
            <h3>Fashion fixie vegan quinoa</h3>
            <p>Chartreuse 90's shoreditch gluten-free cornhole locavore kogi quinoa, cardigan tumblr mlkshk you probably haven't heard of them semiotics.</p>
          </div><!-- /.col-xs-12 col-sm-6 col-md-4 -->

          <div class="col-xs-12 col-sm-6 col-md-6 teaser">
            <img src="<?php echo $imagePath ?>/ui_keyvisual_720.jpg" class="img-responsive" alt="" />
            <h3>Fashion fixie vegan quinoa</h3>
            <p>Chartreuse 90's shoreditch gluten-free cornhole locavore kogi quinoa, cardigan tumblr mlkshk you probably haven't heard of them semiotics.</p>
          </div><!-- /.col-xs-12 col-sm-6 col-md-4 -->

        </div><!-- /.row -->

        <div class="row">
          <div class="col-xs-12 col-sm-6 col-md-3 teaser">
            <img src="<?php echo $imagePath ?>/ui_keyvisual_720.jpg" class="img-responsive" alt="" />
            <h3>Fashion fixie vegan quinoa</h3>
            <p>Chartreuse 90's shoreditch gluten-free cornhole locavore kogi quinoa, cardigan tumblr mlkshk you probably haven't heard of them semiotics.</p>
          </div><!-- /.col-xs-12 col-sm-6 col-md-4 -->

          <div class="col-xs-12 col-sm-6 col-md-3 teaser">
            <img src="<?php echo $imagePath ?>/ui_keyvisual_720.jpg" class="img-responsive" alt="" />
            <h3>Fashion fixie vegan quinoa</h3>
            <p>Chartreuse 90's shoreditch gluten-free cornhole locavore kogi quinoa, cardigan tumblr mlkshk you probably haven't heard of them semiotics.</p>
          </div><!-- /.col-xs-12 col-sm-6 col-md-4 -->

          <div class="col-xs-12 col-sm-6 col-md-3 teaser">
            <img src="<?php echo $imagePath ?>/ui_keyvisual_720.jpg" class="img-responsive" alt="" />
            <h3>Fashion fixie vegan quinoa</h3>
            <p>Chartreuse 90's shoreditch gluten-free cornhole locavore kogi quinoa, cardigan tumblr mlkshk you probably haven't heard of them semiotics.</p>
          </div><!-- /.col-xs-12 col-sm-6 col-md-4 -->

          <div class="col-xs-12 col-sm-6 col-md-3 teaser">
            <img src="<?php echo $imagePath ?>/ui_keyvisual_720.jpg" class="img-responsive" alt="" />
            <h3>Fashion fixie vegan quinoa</h3>
            <p>Chartreuse 90's shoreditch gluten-free cornhole locavore kogi quinoa, cardigan tumblr mlkshk you probably haven't heard of them semiotics.</p>
          </div><!-- /.col-xs-12 col-sm-6 col-md-4 -->

        </div><!-- /.row -->

      </div><!-- /.container -->



      <?php include('src/includes/ui/footer.php'); ?>

  <?php include('src/includes/html/html_footer.php'); ?>
