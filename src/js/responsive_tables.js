////////////////////
// Responsive Tables
// Prepares the markup of responsive tables with the necessary label attributes used by the responsive table CSS code.
var ResponsiveTables = (function($, window, document, undefined) {

	function init() {
		$('.table').each(function () {
			var table = $(this),
				headers = $('thead th', table),
				rows = $('tbody tr', table);

			// for each row where the number of cells matches the number of header cells, set the
			// data-label attribute of the row cell to the text of the header cell at the same position
			rows.each(function () {
				var cells = $('td', this),
					cellIndex;

				if (cells.length === headers.length) {
					for (cellIndex = 0; cellIndex < cells.length; cellIndex++) {
						if (cells.eq(cellIndex).attr('data-label') === undefined) {
							cells.eq(cellIndex).attr('data-label', headers.eq(cellIndex).text());
						}
					}
				}
			});
		});
	}


	// EventEmitter listen for apps init
	ee.addListener('document-ready', init);

	// Public API
	return {
		init: init
	};

})(jQuery, window, document);



