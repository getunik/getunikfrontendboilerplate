///////////////////////////////////
// Scrollspy for UI Sidebar Navigation
var BootstrapInit = (function($, window, document, undefined) {

	function init() {
		// Tooltips from Bootstrap
		$('[data-toggle="tooltip"]').tooltip();

		// Popovers from Bootstrap
		$('[data-toggle="popover"]').popover();
	}

	// EventEmitter listen for apps init
	ee.addListener('document-ready', init);

	// Public API
	return {
		init: init
	};
})(jQuery, window, document);
