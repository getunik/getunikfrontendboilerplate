//////////////////////////
// Full screen start page
var FullscreenJumbo = (function($, window, document, undefined) {

	function init() {
		$('.getu-jumbotron-fullscreen .getu-jumbotron-image-wrapper').height( $(window).height() );
	}

	function resize() {
		$('.getu-jumbotron-fullscreen .getu-jumbotron-image-wrapper').height( $(window).height() );
	}

	// EventEmitter listen for apps window-load
	ee.addListener('window-load', init);
	ee.addListener('window-resize', resize);

	// Public API
	return {
		init: init
	};

})(jQuery, window, document);
