var SlickCarouselModule = (function($, window, document, undefined) {

	function init() {
		$('.getu-carousel').slick({
			lazyLoad: 'ondemand',
			prevArrow: '<a href="" class="slick-prev"></a>',
			nextArrow: '<a href="" class="slick-next"></a>',
			dots: true,
			speed: 300
		});
	}

	// EventEmitter listen for apps init
	ee.addListener('document-ready', init);

	// Public API
	return {
		init: init
	};

})(jQuery, window, document);
