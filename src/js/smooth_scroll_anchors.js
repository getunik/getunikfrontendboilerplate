
var SmoothScrollAnchors = (function($, window, document, undefined) {

	function init() {
		// smooth scroll to anchors
		$('a[href*=#]:not([href=#])').click(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top
					}, 500);
					return false;
				}
			}
		});
	}

	// EventEmitter listen for apps window-load
	ee.addListener('window-load', init);

	// Public API
	return {
		init: init
	};

})(jQuery, window, document);



