var EqualizerModule = (function($, window, document, undefined) {

  function init() {
    // home stats elelemnts equalizer
    $('.js-equalizer-wrapper').equalizer({
      // height = type of height to use
      // "o" or "outer" = "outerHeight" - includes height + padding + border + margin
      // "i" or "inner" = "innerHeight" - includes height + padding + border
      // default        = "height"      - use just the height
      columns    : '.js-equalizer-element',     // elements inside of the wrapper
      useHeight  : 'outerHeight',    // height measurement to use
      resizeable : true,        // when true, heights are adjusted on window resize
      min        : 0,           // Minimum height applied to all columns
      max        : 0,           // Max height applied to all columns
      breakpoint : null,        // if browser less than this width, disable the plugin
      disabled   : 'noresize',  // class applied when browser width is less than the breakpoint value
      overflow   : 'overflowed' // class applied to columns that are taller than the allowable max
    });
  }

  // EventEmitter listen for apps init
  ee.addListener('window-load', init);

  // Public API
  return {
    init: init
  };

})(jQuery, window, document);
