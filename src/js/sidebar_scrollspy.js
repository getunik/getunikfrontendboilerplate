///////////////////////////////////
// Scrollspy for UI Sidebar Navigation
var SidebarScrollspy = (function($, window, document, undefined) {

	function init() {
		$('body').scrollspy({
		    target: '.ui-sidebar-navigation',
		    offset: 200
		});

		$(window).scroll(function() {
			if ($(window).scrollTop() > 240) {
				$('.ui-sidebar-navigation ul').css('position', 'fixed');
			} else {
				$('.ui-sidebar-navigation ul').css('position', 'static');
			}
		});
	}


	// EventEmitter listen for apps init
	ee.addListener('document-ready', init);

	// Public API
	return {
		init: init
	};

})(jQuery, window, document);
