var SvgInjector = (function($, window, document, undefined) {

	function init() {
		var svgs = document.querySelectorAll('img.inject-svg'),
			injectorOptions = {
				evalScripts: false,
				pngFallback: 'images/base', // fallback image folder path for IE8
				each: function (svg) {
					// Callback after each SVG is injected
					//console.log('SVG injected: ');
					//console.log(svg);
				}
			};

			// Trigger the injection
			SVGInjector(svgs, injectorOptions, function (totalSVGsInjected) {
				// Callback after all SVGs are injected
				//console.log('Total injected ' + totalSVGsInjected + ' SVG(s)!');
			});
	}

	// EventEmitter listen for apps init
	ee.addListener('document-ready', init);

	// Public API
	return {
		init: init
	};

})(jQuery, window, document);
