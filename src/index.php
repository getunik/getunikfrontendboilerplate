
    <?php include('src/includes/helpers.php'); ?>
    <?php include('src/includes/html/html_header.php'); ?>

    <body class="dev">

        <div class="getu-jumbotron getu-jumbotron-fullscreen ui-guide-bs-splash">

            <div class="getu-jumbotron-image-wrapper" style="background-image: url(<?php echo $imagePath; ?>ui_guide_bs_3po_1440.jpg); background-position: top center;">

                <div class="getu-jumbotron-tint"></div>

                <div class="getu-jumbotron-image visible-xs">
                    <img class="img-responsive" src="<?php echo $imagePath; ?>ui_guide_bs_3po_720.jpg" alt="Guide_BS">
                </div>

                <div class="getu-jumbotron-title-wrapper">
                    <h1 class="text-center">Guide_BS</h1>
                    <p class="lead text-center">Getunik User Interface Design Essentials for Bootstrap</p><!-- /.lead -->

                    <div class="getu-jumbotron-btn-wrapper text-center">
                        <a href="ui_design_pattern.html" class="btn btn-lg btn-primary btn-primary-inverted">Get started</a>
                    </div>
                </div>

            </div>

        </div>

        <img class="ui-guide-logo" src="<?php echo $imagePath; ?>getunik_logo.svg" alt="" />

    <?php include('src/includes/html/html_footer.php'); ?>
