
  <?php include('src/includes/helpers.php'); ?>
  <?php include('src/includes/html/html_header.php'); ?>

  <body class="dev">

      <?php includeFile('src/includes/ui/top_navi.php', array('activeStep' => 4)); ?>

      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-md-8">

            <h1>Basic Page / Content Page</h1>

            <p class="lead">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>

            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>

            <h2>Pellentesque habitant</h2>

            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>

            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>

            <img class="img-responsive" src="<?php echo $imagePath; ?>/ui_keyvisual_720.jpg" alt="" />

            <ul>
              <li>One shoreditch</li>
              <li>cornhole</li>
              <li>locavore</li>
              <li>humblebrag</li>
              <li>chambray</li>
            </ul>

            <ol>
              <li>One shoreditch</li>
              <li>cornhole</li>
              <li>locavore</li>
              <li>humblebrag</li>
              <li>chambray</li>
            </ol>

          </div><!-- /.col-xs-12 -->
          <div class="col-xs-12 col-md-3 col-md-offset-1">

            <h3>Sidebar</h3>

            <ul>
              <li>One shoreditch</li>
              <li>cornhole</li>
              <li>locavore</li>
              <li>humblebrag</li>
              <li>chambray</li>
            </ul>

            <ol>
              <li>One shoreditch</li>
              <li>cornhole</li>
              <li>locavore</li>
              <li>humblebrag</li>
              <li>chambray</li>
            </ol>

          </div><!-- /.col-xs-12 col-md-4 -->
        </div><!-- /.row -->
      </div><!-- /.container -->

      <?php include('src/includes/ui/footer.php'); ?>

  <?php include('src/includes/html/html_footer.php'); ?>
